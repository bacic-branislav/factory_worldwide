import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    fields: [
      {letter: 'A', value: 3, sign: null, change: true},
      {letter: 'B', value: 3, sign: null, change: true},
      {letter: 'C', value: 3, sign: null, change: true},
      {letter: 'D', value: 3, sign: null, change: true},
      {letter: 'E', value: 3, sign: null, change: true},
      {letter: 'F', value: 3, sign: null, change: true},
      {letter: 'G', value: 3, sign: null, change: true},
      {letter: 'H', value: 3, sign: null, change: true},
      {letter: 'I', value: 3, sign: null, change: true},
      {letter: 'J', value: 3, sign: null, change: true},
    ],
    timestamps: [],
    statistics: [],
  },
  mutations: {
    UPDATE_TIMESTAMPS(state, payload) {
      state.timestamps.push(payload.timestamps)
    },
    UPDATE_STATISTICS(state, payload) {
      state.statistics.push(payload)
    },
  },
  getters: {
    getFields: (state => state.fields),
    getStats: (state => state.statistics)
  }
})
